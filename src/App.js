import "./App.css";

import React, { useState } from "react";

import ElCirculo from "./components/ElCirculo";
import "./components/ElCirculo.css";

function App() {
	const [toggle, setToggle] = useState(false);

	const handleClick = () => {
		setToggle(!toggle);
	};

	return (
		<div className="App">
			<ElCirculo />
		</div>
	);
}

export default App;
