import React, { useState} from "react";

export default function ElCirculo() {
	const [toggleCircle, setToggleCircle] = useState(false);
	const [animateCircle, setAnimateCircle] = useState(0);

	const handleClickCircle = () => {
		setToggleCircle(!toggleCircle);
		renderAnimate();
	};

	const renderAnimate = () => {
		return toggleCircle ? setAnimateCircle(1) : setAnimateCircle(0);
	};

	return (
		<>
			<div className="circlebase circlecontainer">
				<div className="circlebase outercircle">
					<div
						className="circlebase innercircle"
						animation={animateCircle}
					></div>
				</div>
				<div className="innercircletext">
					<b>CIRCLE</b>
					<br />
					<i>animation</i>
				</div>
			</div>

			<button type="button" onClick={handleClickCircle}>
				{" "}
				TOGGLE CIRCLE ANIMATION
			</button>
		</>
	);
}
